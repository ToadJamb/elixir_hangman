defmodule DebugTest do
  use ExUnit.Case, async: true
  doctest Debug

  @described_module Debug

  describe ".type_of" do
    [
      [:foo, :atom],
      ["foo", :binary],
      [1, :number],
      [{}, :tuple],
      [%{}, :map],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)

      test "given #{inspect input}, it returns #{inspect expected}" do
        [input, expected] = unquote(params)
        assert @described_module.type_of(input) == expected
      end
    end)
  end

  describe ".task_id" do
    test "returns a random 6-character string" do
      actual = @described_module.task_id()

      assert is_binary(actual)
      assert String.match?(actual, ~r/\w/)
      assert !String.match?(actual, ~r/-/)
      assert length(String.codepoints(actual)) == 6

      assert actual != @described_module.task_id()
    end
  end

  describe ".task_ids" do
    test "returns random 6-character strings" do
      actual = @described_module.task_ids(5)

      assert is_list(actual)

      assert length(actual) == 5

      actual
      |> Enum.map(fn(id) ->
        assert is_binary(id)
        assert String.match?(id, ~r/\d/)
        assert String.match?(id, ~r/\w/)
        assert !String.match?(id, ~r/-/)
        assert length(String.codepoints(id)) == 6
      end)
    end
  end
end

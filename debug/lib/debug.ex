defmodule Debug do
  @spec show(any) :: any
  def show(var, description \\ "") do
    show_description(description)
    show_type(var)
    IO.puts(inspect(var))
    IO.puts(String.duplicate("-", 80))
    IO.puts(Macro.to_string(var))
    IO.puts(String.duplicate("=", 80))
    var
  end

  @spec type_of(any) :: atom
  def type_of(var) when is_atom(var), do: :atom
  def type_of(var) when is_map(var), do: :map
  def type_of(var) when is_tuple(var), do: :tuple
  def type_of(var) when is_list(var), do: :list
  def type_of(var) when is_number(var), do: :number
  def type_of(var) when is_binary(var), do: :binary
  def type_of(var) when is_function(var), do: :function
  def type_of(_), do: :unknown

  @spec show_task_ids(non_neg_integer) :: :ok
  def show_task_ids(count) do
    task_ids(count)
    |> Enum.each(&IO.puts/1)
  end

  @spec task_id(non_neg_integer) :: String.t
  def task_id(len) do
    SecureRandom.uuid()
    |> String.codepoints()
    |> Enum.filter(&(&1 != "-"))
    |> Enum.take(len)
    |> Enum.join()
  end
  @spec task_id() :: String.t
  def task_id, do: task_id(6)

  @spec task_ids(non_neg_integer) :: list
  def task_ids(count) do
    1..count
    |> Enum.map(fn(_) -> task_id() end)
  end

  @spec show_type(any) :: any
  defp show_type(var) do
    IO.puts "type: #{type_of(var)}"
    var
  end

  @spec show_description(String.t()) :: :ok
  defp show_description(""), do: IO.puts(String.duplicate("=", 80))

  defp show_description(description) do
    header = "=== #{description} "
    trail = String.duplicate("=", 80 - String.length(header))
    IO.puts(header <> trail)
  end
end

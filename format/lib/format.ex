defmodule Format do
  @spec replace_mask(String.t(), list) :: String.t()
  def replace_mask(mask, replacements) do
    Enum.reduce(replacements, mask, fn replacement, s ->
      String.replace(s, "%s", inspect(replacement), global: false)
    end)
  end
end

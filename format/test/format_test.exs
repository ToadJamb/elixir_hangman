defmodule FormatTest do
  use ExUnit.Case, async: true

  doctest Format

  @described_module Format

  describe "#{@described_module}.replace_mask/2" do
    [%{
      input: ["x%sx", ["foo", 3]],
      expected: "x\"foo\"x",
    }, %{
      input: [" %s: %s :: %s ", ["bar", 4, %{foo: :bar}]],
      expected: " \"bar\": 4 :: %{foo: :bar} ",
    }]
    |> Enum.each(fn(params) ->
      quoted_params = Macro.escape(params)

      desc = "given #{inspect(params.input)}, "
      desc = desc <> "it returns #{inspect(params.expected)}"

      test desc do
        %{input: input, expected: expected} = unquote(quoted_params)
        assert Kernel.apply(@described_module, :replace_mask, input) == expected
      end
    end)
  end
end

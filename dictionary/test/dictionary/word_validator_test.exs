defmodule WordValidatorTest do
  use ExUnit.Case, async: true

  doctest Dictionary.WordValidator

  @described_module Dictionary.WordValidator

  describe "#{@described_module}.valid_word?/1" do
    valid_word_args = Macro.escape([
      ["fooba",    true],
      ["foob",     false],
      ["foobar\n", false],
      ["fooBar",   false],
      ["foobar's", false],
      ["foo\tb",   false],
      ["étude",    false],
    ])

    for args <- valid_word_args do
      [input, expected] = args

      test "given #{inspect(input)} returns #{inspect(expected)}" do
        [input, expected] = unquote(args)
        assert @described_module.valid_word?(input) == expected
      end
    end
  end
end

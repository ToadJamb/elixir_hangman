defmodule Dictionary.WordsTest do
  use ExUnit.Case, async: true

  doctest Dictionary.Words

  @described_module Dictionary.Words

  describe "#{@described_module}.random_word/0" do
    test "returns a string" do
      word = @described_module.random_word()

      assert is_binary(word)
      assert word =~ ~r/^\w+$/
    end

    test "returns a single word" do
      word = @described_module.random_word()
      assert [word] == String.split(word)
    end
  end
end

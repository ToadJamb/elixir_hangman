defmodule DictionaryTest do
  use ExUnit.Case, async: true

  doctest Dictionary

  @described_module Dictionary

  describe "#{@described_module}.random_word/0" do
    test "returns a single word" do
      word = @described_module.random_word()
      assert [word] == String.split(word)
    end
  end
end

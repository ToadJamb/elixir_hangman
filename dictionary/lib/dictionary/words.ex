defmodule Dictionary.Words do
  alias Dictionary.WordValidator

  require Logger

  @pid_name __MODULE__

  @spec start_link :: {:ok, pid}
  def start_link do
    Logger.debug fn -> "starting #{__MODULE__}" end
    Agent.start_link &word_list/0, name: @pid_name
  end

  @spec random_word :: String.t()
  def random_word do
    Logger.debug fn -> "random_word start" end

    #if :rand.uniform < 1 / 3 do
    #  Logger.error fn -> "random_word failure" end
    #  Agent.get @pid_name, fn(_) ->
    #    Logger.error fn -> "random_word exit" end
    #    exit :epic_fail
    #  end
    #end

    #Logger.debug fn -> "random_word retrieving" end
    #Agent.get @pid_name, &Enum.random/1
    Agent.get @pid_name, &select_word/1
  end

  @spec select_word(list) :: String.t
  defp select_word(words) do
    Logger.debug fn -> "selecting word" end
    words
    |> Enum.random
  end

  @spec word_list :: list
  defp word_list do
    Logger.debug fn -> "retrieving word list" end
    word_path()
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.filter(&WordValidator.valid_word?/1)
  end

  @spec word_path :: String.t()
  defp word_path do
    "/etc/dictionaries-common/words"
  end
end

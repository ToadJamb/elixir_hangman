defmodule Dictionary.WordValidator do
  @doc """
  iex> Dictionary.WordValidator.valid_word? "foooo"
  true
  iex> Dictionary.WordValidator.valid_word? "Foooo"
  false
  """

  @type check_result :: {:ok, String.t()} | {:error, String.t()}

  @spec valid_word?(String.t()) :: boolean
  def valid_word?(word) do
    word
    |> check(&(&1 == String.downcase(&1)))
    |> check(&(&1 == String.replace(&1, ~r/\s/, "")))
    |> check(&(&1 =~ ~r/^\w+$/))
    |> check(&(&1 |> String.codepoints() |> length > 4))
    |> valid?
  end

  @spec check(check_result, function) :: check_result
  defp check({:ok, word}, fx), do: check(word, fx)
  defp check({:error, word}, _fx), do: {:error, word}

  @spec check(String.t(), function) :: check_result
  defp check(word, fx), do: check_result(fx.(word), word)

  @spec check_result(boolean, String.t()) :: check_result
  defp check_result(true, word), do: {:ok, word}
  defp check_result(false, word), do: {:error, word}

  @spec valid?(check_result | any) :: boolean
  defp valid?({:ok, _word}), do: true
  defp valid?(_), do: false
end

defmodule Dictionary do
  alias Dictionary.Words

  @spec random_word :: String.t()
  defdelegate random_word, to: Words
end

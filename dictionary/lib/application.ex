defmodule Dictionary.Application do
  use Application

  require Logger

  alias Dictionary.Words

  @spec start(:normal, list) :: {:ok, pid}
  def start(_type, _args) do
    Logger.debug fn -> "starting #{__MODULE__}" end
    Supervisor.start_link children(), options()
  end

  @spec children :: list
  defp children do
    import Supervisor.Spec
    [
      worker(Words, []),
    ]
  end

  @spec options :: list
  defp options do
    [
      name: Dictionary.Supervisor,
      strategy: :one_for_one,
    ]
  end
end

defmodule Dictionary.MixProject do
  use Mix.Project

  def project do
    [
      app: :dictionary,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      default_task: "default",
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: { Dictionary.Application, [] },
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      default: [
        "cmd mix test --color",
        "cmd mix credo list --strict",
        "cmd mix dialyzer --halt-exit-status"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 0.9.1", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc", only: [:dev, :test], runtime: false},
      {:logger_file_backend, "~> 0.0"}
    ]
  end
end

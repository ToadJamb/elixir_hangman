defmodule ExercisesTest do
  use ExUnit.Case, async: true
  doctest Exercises

  follows_a_args =
    Macro.escape([
      ["abc", true],
      ["arc", true],
      ["ace", false]
    ])

  for args <- follows_a_args do
    [input, expected] = args

    desc =
      "#follows_a? given #{inspect(input)}, it returns #{inspect(expected)}"

    test desc do
      [input, expected] = unquote(args)
      assert Exercises.follows_a?(input) == expected
    end
  end

  all_cat_with_dog_args =
    Macro.escape([
      ["cat foo cat", "dog foo dog"],
      ["dcatg cat foo", "ddogg dog foo"]
    ])

  for args <- all_cat_with_dog_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#replace_all_cat_with_dog ",
        "given #{inspect(input)}, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      assert Exercises.replace_all_cat_with_dog(input) == expected
    end
  end

  first_cat_with_dog_args =
    Macro.escape([
      ["cat foo cat", "dog foo cat"],
      ["dcatg cat foo", "ddogg cat foo"]
    ])

  for args <- first_cat_with_dog_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#replace_first_cat_with_dog ",
        "given #{inspect(input)}, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      assert Exercises.replace_first_cat_with_dog(input) == expected
    end
  end

  reverse_two_tuple_args =
    Macro.escape([
      [{1, 1}, {1, 1}],
      [{2, 3}, {3, 2}]
    ])

  for args <- reverse_two_tuple_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#reverse_two_tuple ",
        "given #{inspect(input)}, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      assert Exercises.reverse_two_tuple(input) == expected
    end
  end

  same_args =
    Macro.escape([
      [[1, 1], true],
      [["foo", "foo"], true],
      [[{1, 1}, {1, 1}], true],
      [[1, 2], false],
      [[{2, 3}, {3, 2}], false]
    ])

  for args <- same_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#same? ",
        "given #{inspect(input)}, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      assert apply(Exercises, :same?, input) == expected
    end
  end

  describe "#{Exercises}.pythagorean_triples/1" do
    arg_list = [
      [7, [[3, 4, 5]]],
      [10, [[3, 4, 5], [6, 8, 10]]]
    ]

    for args <- arg_list do
      [max, expected] = args

      test "given #{max}, it returns #{expected}" do
        [max, expected] = unquote(args)
        assert Exercises.pythagorean_triples(max) == expected
      end
    end
  end
end

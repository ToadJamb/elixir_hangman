defmodule ListsTest do
  use ExUnit.Case, async: true
  doctest Lists

  len_args =
    Macro.escape([
      [[], 0],
      [[1], 1],
      [[1, 2], 2],
      [[1, 2, 3, 4, 8], 5]
    ])

  for args <- len_args do
    [input, expected] = args

    desc = "#len given #{inspect(input)}, it returns #{inspect(expected)}"

    test desc do
      [input, expected] = unquote(args)
      assert apply(Lists, :len, [input]) == expected
    end
  end

  sum_args =
    Macro.escape([
      [[], 0],
      [[1], 1],
      [[1, 2], 3],
      [[1, 2, 3, 4, 8], 18]
    ])

  for args <- sum_args do
    [input, expected] = args

    desc = "#sum given #{inspect(input)}, it returns #{inspect(expected)}"

    test desc do
      [input, expected] = unquote(args)
      assert apply(Lists, :sum, [input]) == expected
    end
  end

  double_args =
    Macro.escape([
      [[1], [2]],
      [[1, 2], [2, 4]],
      [[1, 2, 3, 4, 8], [2, 4, 6, 8, 16]]
    ])

  for args <- double_args do
    [input, expected] = args

    desc = "#double given #{inspect(input)}, it returns #{inspect(expected)}"

    test desc do
      [input, expected] = unquote(args)
      assert apply(Lists, :double, [input]) == expected
    end
  end

  square_args =
    Macro.escape([
      [[1], [1]],
      [[1, 2], [1, 4]],
      [[1, 2, 3, 4, 8], [1, 4, 9, 16, 64]]
    ])

  for args <- square_args do
    [input, expected] = args

    desc = "#square given #{inspect(input)}, it returns #{inspect(expected)}"

    test desc do
      [input, expected] = unquote(args)
      assert apply(Lists, :square, [input]) == expected
    end
  end

  for args <- double_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#map given #{inspect(input)} and a doubling function, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      fx = fn x -> 2 * x end
      assert apply(Lists, :map, [input, fx]) == expected
    end
  end

  for args <- square_args do
    [input, expected] = args

    desc =
      Enum.join([
        "#map given #{inspect(input)} and a squaring function, ",
        "it returns #{inspect(expected)}"
      ])

    test desc do
      [input, expected] = unquote(args)
      fx = fn x -> x * x end
      assert apply(Lists, :map, [input, fx]) == expected
    end
  end
end

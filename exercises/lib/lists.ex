defmodule Lists do
  @spec len(list) :: non_neg_integer
  def len([]), do: 0
  def len([_h | t]), do: len(t) + 1

  @spec sum(list) :: number
  def sum([]), do: 0
  def sum([h | t]), do: h + sum(t)

  @spec double(list | number) :: list | number
  def double([]), do: []
  def double([h | t]), do: [double(h) | double(t)]
  def double(a), do: a * 2

  @spec square(list) :: list
  def square(list) do
    #fx = fn(n) -> n * n end
    #map list, fx
    map(list, &(&1 * &1))
  end

  @spec map(list, function) :: list
  def map([], _fx), do: []
  def map([h | t], fx), do: [fx.(h) | map(t, fx)]
end

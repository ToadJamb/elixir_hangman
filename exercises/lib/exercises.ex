defmodule Exercises do
  @spec follows_a?(String.t()) :: boolean
  def follows_a?(string) do
    string
    |> String.match?(~r/a.c/)
  end

  @spec replace_all_cat_with_dog(String.t()) :: String.t()
  def replace_all_cat_with_dog(string) do
    string
    |> String.replace("cat", "dog")
  end

  @spec replace_first_cat_with_dog(String.t()) :: String.t()
  def replace_first_cat_with_dog(string) do
    string
    |> String.replace("cat", "dog", global: false)
  end

  @spec reverse_two_tuple({any, any}) :: {any, any}
  def reverse_two_tuple({first, last}) do
    {last, first}
  end

  @spec same?(any, any) :: boolean
  def same?(a, a), do: true
  def same?(_a, _b), do: false

  @type pythagorean_triples :: list

  @spec pythagorean_triples :: pythagorean_triples
  def pythagorean_triples, do: pythagorean_triples(100)
  @spec pythagorean_triples(non_neg_integer) :: pythagorean_triples
  # credo:disable-for-next-line Credo.Check.Refactor.ABCSize
  def pythagorean_triples(max) do
    for a <- 1..max,
        b <- (a + 1)..max,
        c <- (b + 1)..max,
        a * a + b * b == c * c,
        do: [a, b, c]
  end
end

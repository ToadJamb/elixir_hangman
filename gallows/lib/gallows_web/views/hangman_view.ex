defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  @statuses %{
    :initializing => %{
      :class   => "primary",
      :message => "Enter a letter to get started. Good luck!",
    },
    :won => %{
      :class   => "success",
      :message => "Winner, winner, chicken dinner!",
    },
    :lost => %{
      :class   => "danger",
      :message => "Better luck next time... LOSER!",
    },
    :already_guessed => %{
      :class   => "info",
      :message => "You already guessed that letter.",
    },
    :invalid_guess => %{
      :class   => "info",
      :message => "That is not a valid guess.",
    },
    :bad_guess => %{
      :class   => "warning",
      :message => "That letter is not in the word.",
    },
    :good_guess => %{
      :class   => "primary",
      :message => "Good guess!",
    },
  }

  @valid_statuses Map.keys(@statuses)

  def join_letters(letters), do: Enum.join(letters, " ")

  def show_status?(status) when status in @valid_statuses, do: true
  def show_status?(_), do: false

  def alert_class(status) do
    @statuses[status].class
  end

  def status_message(status) do
    @statuses[status].message
  end

  def turn(turns, target) when target < turns, do: "opacity: 0.1"
  def turn(_turns, _target), do: "opacity: 1"
end

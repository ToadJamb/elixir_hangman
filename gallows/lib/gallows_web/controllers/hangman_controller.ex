defmodule GallowsWeb.HangmanController do
  use GallowsWeb, :controller

  def index(conn, _params) do
    render_new_game conn
  end

  def new(conn, _params) do
    render_new_game conn
  end

  def update(conn, params) do
    with %{"move" => %{"letter" => letter}} <- params,
         pid <- get_session(conn, :pid),
         game <- Hangman.make_guess(pid, letter) do

      conn
      |> reset_guess(game.state)
      |> assign(:game, game)
      |> render("index.html")
    end
  end

  defp render_new_game(conn) do
    with {pid, game} <- Hangman.new_game do
      conn
      |> put_session(:pid, pid)
      |> assign(:game, game)
      |> render("index.html")
    end
  end

  defp reset_guess(conn, :invalid_guess), do: conn
  defp reset_guess(conn, _status), do: put_in conn.params["move"]["letter"], ""
end

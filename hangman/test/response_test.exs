defmodule ResponseTest do
  use ExUnit.Case, async: true
  doctest Hangman.Response

  alias Hangman.Game
  alias Hangman.Response

  @described_module Hangman.Response

  describe "#{@described_module}.new/0" do
    test "returns a response" do
      response = @described_module.new()
      game     = response.game

      assert %@described_module{} = response

      assert %Game{} = game

      assert response == %@described_module{
        game:    game,
        word:    Enum.join(game.letters),
        letters: List.duplicate("_", length(game.letters)),
        turns:   game.turns,
        used:    MapSet.new(),
        state:   game.state,
      }
    end
  end

  describe "#{@described_module}.new/1 given a #{Game}" do
    arg_list = [
      [
        Game.new(%{
          letters: ["f", "o", "o"],
          turns: 3,
          used: ["x", "y"]
        }),
        %{
          state: :initializing,
          word: "foo",
          letters: ["_", "_", "_"],
          turns: 3,
          used: MapSet.new(["x", "y"])
        }
      ],
      [
        Game.new(%{
          letters: ["f", "o", "o"],
          state: :foo,
          turns: 4,
          used: ["o", "y"]
        }),
        %{
          word: "foo",
          state: :foo,
          letters: ["_", "o", "o"],
          turns: 4,
          used: MapSet.new(["o", "y"])
        }
      ]
    ]

    #for args <- arg_list do
    Enum.each(arg_list, fn args ->
      [input, expected] = args

      desc =
        "given %s turns, returns %s"
        |> Format.replace_mask([
          input.turns,
          expected
        ])

      test_args = Macro.escape(args)

      test desc do
        [input, expected] = unquote(test_args)
        actual = @described_module.new(input)

        expected_map = Map.merge(expected, %{game: input})

        assert actual == %Response{} |> Map.merge(expected_map)
      end

      Map.keys(expected)
      |> Enum.each(fn
        key when key == :game -> nil
        key ->
          desc =
            "given %s, %s returns %s"
            |> Format.replace_mask([
              input,
              key,
              expected[key]
            ])

          test_args = Macro.escape(args)

          test desc do
            [input, expected] = unquote(test_args)
            key = unquote(key)
            actual = get_in(@described_module.new(input), [Access.key!(key)])
            assert actual == expected[key]
          end
      end)
    end)
  end

  describe "#{@described_module}.new/1 given a #{@described_module}" do
    arg_list = [
      [
        %Response{
          game: Game.new(%{
            letters: ["f", "o", "o"],
            turns: 3,
            used: ["x", "y"]
          })
        }, %{
          state: :initializing,
          word: "foo",
          letters: ["_", "_", "_"],
          turns: 3,
          used: MapSet.new(["x", "y"])
        }
      ],
      [
        %Response{
          game: Game.new(%{
            letters: ["f", "o", "o"],
            state: :won,
            turns: 4,
            used: ["o", "y"]
          }),
        }, %{
          word: "foo",
          state: :won,
          letters: ["_", "o", "o"],
          turns: 4,
          used: MapSet.new(["o", "y"])
        }
      ],
      [
        %Response{
          game: Game.new(%{
            letters: ["f", "o", "o"],
            state: :won,
            turns: 5,
            used: ["o", "y"]
          })} |> Map.merge(%{foo: "bar"}), %{
            word: "foo",
            state: :won,
            letters: ["_", "o", "o"],
            turns: 5,
            used: MapSet.new(["o", "y"]),
            foo: "bar",
          }
      ]
    ]

    # for args <- arg_list do
    Enum.each(arg_list, fn args ->
      [input, expected] = args

      desc =
        "given %s turns, returns %s"
        |> Format.replace_mask([
          input.game.turns,
          expected
        ])

      test_args = Macro.escape(args)

      test desc do
        [input, expected] = unquote(test_args)
        actual = @described_module.new(input)

        game = input.game
        expected_map = Map.merge(expected, %{game: game})

        assert actual == %Response{} |> Map.merge(expected_map)
      end

      Map.keys(expected)
      |> Enum.each(fn
        key when key == :game -> nil
        key ->
          desc =
            "given %s, %s returns %s"
            |> Format.replace_mask([
              input.game,
              key,
              expected[key]
            ])

          test_args = Macro.escape(args)

          test desc do
            [input, expected] = unquote(test_args)
            key = unquote(key)
            actual = get_in(@described_module.new(input), [Access.key!(key)])
            assert actual == expected[key]
          end
      end)
    end)
  end
end

defmodule HangmanTest do
  use ExUnit.Case, async: true
  doctest Hangman

  alias Hangman.Game
  alias Hangman.Response

  @described_module Hangman

  @moduletag timeout: 3_000

  describe "#{@described_module}.new_game/0" do
    test "returns a response that contains a game" do
      {pid, response} = @described_module.new_game()

      assert is_pid(pid)

      assert %Response{} = response
      assert %Game{} = response.game
    end
  end

  describe "#{@described_module}.make_guess/2" do
    test "returns a response that contains a game" do
      {pid, original_response} = @described_module.new_game()

      response = @described_module.make_guess(pid, "xx")

      assert %Response{} = response
      assert %Game{} = response.game

      assert response != original_response
      assert response.game != original_response.game
    end
  end
end

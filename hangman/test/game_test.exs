defmodule GameTest do
  use ExUnit.Case, async: true
  doctest Hangman.Game

  alias Hangman.Response

  @described_module Hangman.Game

  describe "#{@described_module}.new/0" do
    test "returns a map with the expected keys" do
      game = @described_module.new()

      assert is_map(game) == true

      assert Map.keys(game) == [
        :__struct__,
        :letters,
        :state,
        :turns,
        :used,
      ]
    end

    test "returns a map that contains a key with a letters list" do
      game = @described_module.new()

      assert length(game.letters) > 4
      assert Enum.all?(game.letters, &(&1 =~ ~r/^\w$/))
      assert Enum.all?(game.letters, &(length(String.codepoints(&1)) == 1))
    end

    test "returns a game with a new word" do
      game = @described_module.new()
      assert length(game.letters) > 0
    end

    initial_state = %{
      state: :initializing,
      turns: 7,
      used: Macro.escape(MapSet.new())
    }

    for attr <- Enum.map(initial_state, & &1) do
      {key, expected} = attr

      test "returns a map with %{#{key}: #{Macro.to_string(expected)}}" do
        {key, expected} = unquote(attr)

        game = @described_module.new()
        actual = get_in(game, [Access.key(key)])

        assert actual == expected
      end
    end
  end

  describe "#{@described_module}.make_guess/2 with a won state" do
    test "does not change the game" do
      response =
        Response.new(%Response{
          game: @described_module.new(%{state: :won})
        })

      %Response{game: game} = @described_module.make_guess(response, "x")
      assert game == response.game
    end
  end

  describe "#{@described_module}.make_guess/2 with a lost state" do
    test "does not change the game" do
      response =
        Response.new(%Response{
          game: @described_module.new(%{state: :lost})
        })

      %Response{game: game} = @described_module.make_guess(response, "x")
      assert game == response.game
    end
  end

  describe "#{@described_module}.make_guess/2" do
    arg_list = Macro.escape [
      [
        %{
          state: :won,
          turns: 3,
          used: ["x", "y"],
        }, "z", %{
          state: :won,
          turns: 3,
          used: ["x", "y"],
        },
      ], [
        %{
          state: :lost,
          turns: 3,
          used: ["x", "y"],
        }, "z", %{
          state: :lost,
          turns: 3,
          used: ["x", "y"],
        },
      ], [
        %{
          turns: 3,
          letters: ["f", "o", "o"],
          used: ["x", "y"],
        }, "z", %{
          state: :bad_guess,
          turns: 2,
          used: ["x", "y", "z"],
        },
      ], [
        %{
          turns: 3,
          letters: ["f", "o", "o"],
          used: ["x", "y"],
        }, "x", %{
          state: :already_guessed,
          turns: 3,
          used: ["x", "y"],
        },
      ], [
        %{
          turns: 3,
          letters: ["f", "o", "o"],
          used: ["x", "y"],
        }, "o", %{
          state: :good_guess,
          turns: 3,
          used: ["x", "y", "o"],
        },
      ], [
        %{
          turns: 1,
          letters: ["f", "o", "o"],
          used: ["a", "b"],
        }, "c", %{
          state: :lost,
          turns: 0,
          used: ["a", "b", "c"],
        },
      ], [
        %{
          turns: 3,
          state: :initializing,
          letters: ["f", "o", "o"],
          used: ["a", "b", "o"],
        }, "f", %{
          state: :won,
          turns: 3,
          used: ["a", "b", "o", "f"],
        },
      ], [
        %{
          turns: 3,
          state: :good_guess,
          letters: ["f", "o", "o"],
          used: ["a", "b", "o"],
        }, "Z", %{
          state: :invalid_guess,
          turns: 3,
          used: ["a", "b", "o"],
        },
      ], [
        %{
          turns: 4,
          state: :bad_guess,
          letters: ["f", "o", "o"],
          used: ["a", "b", "o"],
        }, "an", %{
          state: :invalid_guess,
          turns: 4,
          used: ["a", "b", "o"],
        },
      ], [
        %{
          turns: 2,
          state: :bad_guess,
          letters: ["f", "o", "o"],
          used: ["a", "b", "o"],
        }, "", %{
          state: :invalid_guess,
          turns: 2,
          used: ["a", "b", "o"],
        },
      ], [
        %{
          turns: 2,
          state: :bad_guess,
          letters: ["f", "o", "o"],
          used: ["a", "b", "o"],
        }, "\n", %{
          state: :invalid_guess,
          turns: 2,
          used: ["a", "b", "o"],
        },
      ],
    ]

    for args <- arg_list do
      [state, letter, expected] = args

      desc = "given #{Macro.to_string(state)} " \
             <> "and a guess of #{inspect letter}, " \
             <> "returns #{Macro.to_string(expected)}"

      test desc do
        [state, letter, expected] = unquote(args)

        expected_map = Map.put(expected, :used, MapSet.new(expected.used))

        original_game = @described_module.new(state)

        %Response{game: game} =
          @described_module.make_guess(Response.new(original_game), letter)

        validate = &assert get_in(game, [Access.key(&1)]) == expected_map[&1]

        expected_map
        |> Map.keys()
        |> Enum.each(validate)

        assert Map.keys(game) == Map.keys(@described_module.new())
      end
    end
  end
end

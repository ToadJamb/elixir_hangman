defmodule Hangman.Game do
  use SafeStruct

  alias Hangman.Game
  alias Hangman.Response

  defstruct(
    turns:   7,
    state:   :initializing,
    letters: [],
    used:    MapSet.new()
  )

  @type t :: %__MODULE__{
    turns: number,
    letters: list(String.t),
    state: atom,
    used: %MapSet{},
  }

  @type bases :: none | Game.t()

  @type guess_tuple :: {struct, String.t(), boolean, boolean}

  @spec new(map) :: t
  def new(%{} = attrs) do
    new(%Hangman.Game{}, attrs, %{used: &MapSet.new/1})
  end

  @spec new :: t
  def new do
    new(%{letters: random_word_as_letters()})
  end

  @spec make_guess(Response.t, String.t) :: Response.t
  def make_guess(%Response{
    game: %Game{state: state}
  } = response, _guess)
  when state in [:won, :lost], do: Response.new(response.game)
  def make_guess(response, letter) do
    response.game
    |> build_guess(letter)
    |> apply_guess()
    |> Response.new()
  end

  @spec random_word_as_letters :: nonempty_list(String.t())
  defp random_word_as_letters do
    Dictionary.random_word()
    |> String.codepoints()
  end

  @spec build_guess(t, String.t()) :: guess_tuple
  defp build_guess(game, letter) do
    {
      game,
      letter,
      Enum.member?(game.used, letter),
      letter =~ ~r/\A[a-z]\Z/
    }
  end

  @spec apply_guess(guess_tuple) :: t
  defp apply_guess({game, _l, _g, _valid? = false} = _data) do
    game
    |> Map.put(:state, :invalid_guess)
  end

  defp apply_guess({game, _letter, _already_guessed = true, _g} = _data) do
    game
    |> Map.put(:state, :already_guessed)
  end

  defp apply_guess({game, letter, _already_guessed, _g} = _data) do
    game
    |> Map.put(:used, MapSet.put(game.used, letter))
    |> score_guess(is_found?(game, letter))
  end

  @spec is_found?(t, String.t()) :: boolean
  defp is_found?(game, letter) do
    game.letters
    |> Enum.member?(letter)
  end

  @spec score_guess(t, boolean) :: t
  defp score_guess(game, _has_letter = true) do
    game
    |> Map.put(:state, :good_guess)
    |> final_state
  end

  defp score_guess(game, _has_letter) do
    game
    |> Map.put(:state, :bad_guess)
    |> Map.put(:turns, game.turns - 1)
    |> final_state
  end

  @spec final_state(t) :: t
  defp final_state(%Game{state: :bad_guess, turns: 0} = game) do
    Map.put(game, :state, :lost)
  end

  defp final_state(%Game{state: :bad_guess} = game), do: game

  defp final_state(game) do
    game
    |> set_won(MapSet.subset?(MapSet.new(game.letters), game.used))
  end

  @spec set_won(t, boolean) :: t
  defp set_won(game, _is_subset? = false), do: game

  defp set_won(game, _is_subset? = true) do
    Map.put(game, :state, :won)
  end
end

defmodule Hangman.Response do
  use SafeStruct

  alias Hangman.Game
  alias Hangman.Response

  defstruct(
    game:    %Game{},
    word:     "",
    turns:     0,
    state:   :initializing,
    letters: [],
    used:    MapSet.new()
  )

  @type string_list :: list

  @type t :: %__MODULE__{
    game: Game.t,
    word: String.t,
    turns: non_neg_integer,
    state: atom,
    letters: string_list,
    used: string_list,
  }

  @type bases :: none | Game.t() | Response.t()

  @spec new() :: t
  def new, do: new(%Response{game: Game.new()})

  @spec new(Response.t() | Game.t()) :: t
  def new(%Game{} = game), do: new(%Response{game: game})

  def new(%Response{game: game} = response) do
    new response, %{
      state:   game.state,
      word:    Enum.join(game.letters),
      letters: filtered_letters_for(game.letters, game.used),
      turns:   game.turns,
      used:    game.used,
    }
  end

  @spec filtered_letters_for(string_list, %MapSet{}) :: string_list
  defp filtered_letters_for(letters, used) do
    letters
    |> Enum.map(&revealed_letter(&1, Enum.member?(used, &1)))
  end

  @spec revealed_letter(String.t(), boolean) :: String.t()
  defp revealed_letter(letter, _is_guessed? = true), do: letter
  defp revealed_letter(_letter, _is_guessed?), do: "_"
end

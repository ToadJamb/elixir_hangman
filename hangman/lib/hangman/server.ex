defmodule Hangman.Server do
  use GenServer

  require Logger

  alias Hangman.Game
  alias Hangman.Response

  @spec start_link :: {:ok, pid}
  def start_link do
    Logger.info fn -> "Starting #{__MODULE__}" end
    GenServer.start_link __MODULE__, nil
  end

  @spec init(nil) :: {:ok, Response.t}
  def init(_) do
    Logger.info fn -> "Initializing #{__MODULE__}" end
    response = Response.new()
    Logger.debug fn -> "Game initialized: #{inspect response}" end
    {:ok, response}
  end

  @spec handle_call({:make_guess, String.t}, {pid, any}, Response.t)
  :: {:reply, Response.t, Response.t}
  def handle_call({:make_guess, guess}, _from, response) do
    Logger.debug fn -> "make_guess: #{inspect guess} :: #{inspect response}" end
    new_response = Game.make_guess(response, guess)
    Logger.debug fn -> "response: #{inspect new_response}" end
    {:reply, new_response, new_response}
  end

  @spec handle_call({:get, nil}, {pid, any}, Response.t)
  :: {:reply, Response.t, Response.t}
  def handle_call({:get, nil}, _from, response) do
    Logger.debug fn -> "get response: #{inspect response}" end
    {:reply, response, response}
  end
end

defmodule Hangman do
  require Logger

  alias Hangman.Response

  @spec new_game :: {pid, Response.t}
  def new_game do
    Logger.debug fn -> "new game" end
    {:ok, pid} = Supervisor.start_child(Hangman.Supervisor, [])
    response = GenServer.call(pid, {:get, nil})
    {pid, response}
  end

  @spec make_guess(pid, String.t()) :: Response.t()
  def make_guess(pid, guess) do
    GenServer.call pid, {:make_guess, guess}
  end
end

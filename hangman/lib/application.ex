defmodule Hangman.Application do
  use Application

  require Logger

  @spec start(:normal, list) :: {:ok, pid}
  def start(_type, _args) do
    Logger.debug fn -> "starting #{__MODULE__}" end
    Supervisor.start_link children(), options()
  end

  @spec children :: list
  defp children do
    import Supervisor.Spec, warn: false
    [
      worker(Hangman.Server, []),
    ]
  end

  @spec options :: list
  defp options do
    [
      name: Hangman.Supervisor,
      strategy: :simple_one_for_one,
    ]
  end
end

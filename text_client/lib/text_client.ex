defmodule TextClient.State do
  use SafeStruct

  alias Hangman.Response
  alias TextClient.State

  defstruct(
    pid:      nil,
    response: nil,
    guess:    ""
  )

  @type t :: %__MODULE__{
    pid: pid,
    response: Response.t,
    guess: String.t,
  }

  @type bases :: map

  @spec new(map) :: t
  def new(map) do
    new %State{}, map
  end
end

defmodule TextClient do
  alias TextClient.Core
  @spec start :: no_return
  defdelegate start, to: Core
end

defmodule TextClient.Core do
  alias Hangman.Response
  alias TextClient.Display
  alias TextClient.Prompt
  alias TextClient.State

  @spec start :: no_return
  def start do
    initialize()
    |> play()
  end

  @spec initialize :: State.t()
  def initialize do
    Hangman.new_game()
    |> transform_response()
  end

  @spec play(State.t()) :: no_return
  defp play(state) do
    state
    #|> Debug.show()
    |> Display.show()
    |> Prompt.get("Please enter a guess: ")
    |> make_guess()
    |> transform_response()
    |> finalize()
    |> play()
  end

  @spec transform_response({pid, Response.t()}) :: State.t()
  defp transform_response({pid, %Response{} = response}) do
    #response |> Debug.show("response?")
    State.new(%{pid: pid, response: response})
  end

  @spec make_guess(State.t()) :: {pid, Response.t()}
  defp make_guess(%State{pid: pid, guess: letter}) do
    {pid, Hangman.make_guess(pid, letter)}
  end

  defp finalize(%State{response: %{state: :won} = response}) do
    IO.puts(
      "Congratulations! You Won! The word was #{inspect(response.word)}."
    )

    exit(:normal)
  end

  defp finalize(%State{response: %{state: :lost} = response}) do
    IO.puts("Sorry, you lost. The word was #{inspect(response.word)}.")
    exit(:normal)
  end

  @spec finalize(State.t()) :: State.t()
  defp finalize(state), do: state
end

defmodule TextClient.Display do
  alias TextClient.State

  @spec show(State.t()) :: State.t()
  def show(%State{response: response} = state) do
    IO.puts([
      "\n",
      "letters: #{response.letters}\n",
      "guessed: #{Enum.join(response.used, ",")}\n",
      "turns: #{response.turns}\n",
      format_status(response)
    ])

    state
  end

  @spec format_status(%Hangman.Response{state: atom}) :: String.t()
  defp format_status(%{state: :initializing}), do: ""

  defp format_status(%{state: :good_guess}),
    do: "Good guess!"

  defp format_status(%{state: :bad_guess}),
    do: "Sorry, that letter is not in the word."

  defp format_status(%{state: :already_guessed}),
    do: "You already guessed that letter."

  defp format_status(%{state: :invalid_guess}),
    do: "Only single lowercase letters are accepted as guesses"
end

defmodule TextClient.Prompt do
  alias TextClient.State

  @spec get(State.t(), String.t()) :: State.t()
  def get(%State{} = state, message) do
    IO.gets(message)
    |> process_input(state.response.word)
    |> set_value(state)
  end

  @spec process_input(:eof, String.t()) :: no_return
  defp process_input(:eof, word) do
    IO.puts("It looks like you gave up. The word was #{inspect(word)}")
    exit(:normal)
  end

  @spec process_input({:error, String.t()}, String.t()) :: no_return
  defp process_input({:error, reason}, word) do
    IO.puts("An error occurred: #{reason}")
    IO.puts("The word was #{inspect(word)}")
    exit(:normal)
  end

  @spec process_input(String.t(), String.t()) :: String.t()
  defp process_input(input, _word) do
    input
    |> String.trim()
  end

  @spec set_value(String.t(), State.t()) :: State.t()
  defp set_value(letter, state) do
    State.set_value(state, :guess, letter)
  end
end

defmodule SafeStruct do
  @moduledoc """
  This module provides more robust struct manipulation.
  """

  defmacro __using__(_) do
    quote do
      @spec new(bases, map, map) :: t
      defp new(%{__struct__: type} = struct, attrs, fxs \\ %{}) do
        attrs
        |> Map.keys()
        |> Enum.reduce(struct, map_elements(attrs, fxs))
      end

      @spec set_value(t, atom, any) :: t
      def set_value(%{__struct__: type} = struct, key, value) do
        put_in(struct, [Access.key!(key)], value)
      end

      @spec map_elements(map, map) :: function
      defp map_elements(attrs, fxs) do
        fn key, acc ->
          value = if fxs[key], do: fxs[key].(attrs[key]), else: attrs[key]
          put_in(acc, [Access.key!(key)], value)
        end
      end
    end
  end
end
